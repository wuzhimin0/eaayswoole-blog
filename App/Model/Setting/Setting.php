<?php

namespace App\Model\Setting;

use EasySwoole\ORM\ {
    AbstractModel,
    Utility\Schema\Table,
};
use App\Model\Base;

/**
 * 系统配置
 * Class Setting
 */
class Setting extends Base
{

    protected $tableName = 'setting';

    /**
     * 表的字段
     * @return Table
     */
    public function schemaInfo(bool $isCache = true): Table
    {
        $table = new Table($this->tableName);
        $table->colTinyInt('id')->setIsPrimaryKey(true);
        $table->colVarChar('skey', 100);
        $table->colVarChar('vals', 255);
        $table->colVarChar('descs', 255);
        /*
        create table setting (
            id tinyint(3) unsigned auto_increment,
            skey varchar(100) comment '设置字段名',
            vals varchar(255) comment '设置值',
            descs varchar(255) comment '描述',
            primary key(id)
        ) engine = innodb comment '系统设置';
        insert into setting (`skey`,vals,descs) values ('title','', '系统名称'),
        ('redis_enable', '2', 'redis缓存启动'),
        ('redis_host', '', 'redisHost'),
        ('redis_port', '', 'redis端口'),
        ('redis_password', '', 'redis密码');
        */
        return $table;
    }
}