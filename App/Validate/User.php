<?php

namespace App\Validate;

use App\Common\Tools;
use App\Validate\CustomRule\PasswordRule;

/**
 * @description: 用户验证
 * @param {*}
 * @return {*}
 */
class User extends Base
{
    // 验证规则
    public $rules = [
        'username'   => 'required',
    ];

    // 验证错误消息提示
    public $messages = [
        'username.required'  => '用户名不能为空！',
    ];

    // 验证字段的别名
    public $alias = [
        'username'   => '用户名',
    ];

    /**
     * @description: 特殊规则
     * @param Validate $validate 验证器
     * @param array $data 验证数据
     * @return {*}
     */
    public function special($validate, $data)
    {
        if (!Tools::hasValue($data, 'id')) {
            $validate->addColumn('password')->required('密码不能为空');
        }
        $validate->addFunction(new PasswordRule(), false);
        $msg = '密码必须包括数字、大小写字母、特殊字符且在8-16位';
        $validate->addColumn('password')->callUserRule(new PasswordRule(), $msg);
        return $validate;
    }
}