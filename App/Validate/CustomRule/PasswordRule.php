<?php

namespace App\Validate\CustomRule;

use EasySwoole\Validate\Validate;
use EasySwoole\Validate\Functions\AbstractValidateFunction;

class PasswordRule extends AbstractValidateFunction
{
    /**
     * 返回当前校验规则的名字
     */
    public function name(): string
    {
        return 'password';
    }

    /**
     * 验证失败返回 false，或者用户可以抛出异常，验证成功返回 true
     * @param $itemData
     * @param $arg
     * @param $column
     * @return bool
     */
    public function validate($itemData, $arg, $column, Validate $validate): bool
    {
        $regular = '/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,16}$/';
        if ($itemData && !preg_match($regular, $itemData)) {
            return false;
        }
        return true;
    }
}
