<?php

namespace App\HttpController\Admin;

use EasySwoole\EasySwoole\Task\TaskManager;

use App\Task\CacheDel;
use App\Common\Tools;
use App\Common\Cache\LinkCache;
use App\Model\Article\FriendLink;
use App\Common\Traits\Admin\ {
    Lists as AdminList,
    Info as AdminInfo,
    Create as AdminCreate,
    Delete as AdminDelete,
    Modify as AdminModify,
};
use App\Validate\Link as ValidateLink;

class Link extends Base
{
    use AdminList;
    use AdminInfo;
    use AdminCreate;
    use AdminDelete;
    use AdminModify;

    /**
     * @description: 模型
     * @var Model
     */
    public $model = FriendLink::class;

    /**
     * @description: 主键
     * @var 
     */
    public $id = 'id';

    /**
     * @description: 列表查询条件构建
     * @param array $param 前端参数
     * @return {*}
     */
    protected function indexWhere(array $param)
    {
        $where = [];
        Tools::conditionQuery($where, $param, 'name', 'name', 'like');
        Tools::conditionQuery($where, $param, 'links', 'links', 'like');
        Tools::conditionQuery($where, $param, 'display', 'display', '=');
        return $where;
    }

    /**
     * @description: 列表查询条件默认字段
     * @param {*}
     * @return {*}
     */
    protected function indexDeafultParam()
    {
        return [
            'name'    => '',
            'links'   => '',
            'display' => '',
        ];
    }

    /**
     * @description: 验证创建参数
     * @param array $param 前端参数
     * @return {*}
     */
    protected function validateCreate(array $param)
    {
        return (new ValidateLink)->validate($param);
    }

    /**
     * @description: 创建后
     * @param array $param 前端参数
     * @return {*}
     */
    protected function afterCreate(array $param)
    {
        $task = TaskManager::getInstance();
        $task->async(new CacheDel(['call' => [new LinkCache, 'del'], 'args' => []]));
        return true;
    }

    /**
     * @description: 修改显示属性后
     * @param array $param 前端参数
     * @return {*}
     */
    protected function afterMofidyDisplay(array $param)
    {
        $task = TaskManager::getInstance();
        $task->async(new CacheDel(['call' => [new LinkCache, 'del'], 'args' => []]));
        return true;
    }

    /**
     * @description: 删除后
     * @param array $param 前端参数
     * @return {*}
     */
    protected function beforeDelete(array $param)
    {
        $task = TaskManager::getInstance();
        $task->async(new CacheDel(['call' => [new LinkCache, 'del'], 'args' => []]));
        return true;
    }
}
