<?php

namespace App\HttpController\Admin;

use Exception;
use EasySwoole\ORM\DbManager;
use EasySwoole\EasySwoole\Task\TaskManager;

use App\Task\CacheDel;
use App\Common\Tools;
use App\Model\Article\ {
    Comments as CommentsModel,
    Article as ArticleModel,
};
use App\Common\Cache\CommentCache;
use App\Common\Traits\Admin\ {
    Lists as AdminList,
    Delete as AdminDelete,
};

use App\Validate\Comments as CommentsValidate;

class Comments extends Base
{
    use AdminList;
    use AdminDelete;

    /**
     * @description: 模型
     * @var Model
     */
    public $model = CommentsModel::class;

    /**
     * @description: 主键
     * @var 
     */
    public $id = 'id';

    /**
     * @description: 列表查询条件构建
     * @param array $param 前端参数
     * @return {*}
     */
    protected function indexWhere(array $param)
    {
        $where = [];
        if (Tools::hasValue($param, 'title')) {
            $ids = DbManager::getInstance()->invoke(function ($client) use ($param) {
                $model = ArticleModel::invoke($client);
                return $model->where('name', '%' . $param['title'] . '%', 'like')
                    ->column('id');
            });
            $where['article_id'] = $ids ? array($ids, 'in') : 0;
        }
        Tools::conditionQuery($where, $param, 'state', 'state', '=');
        return $where;
    }

    /**
     * @description: 列表查询条件默认字段
     * @param {*}
     * @return {*}
     */
    protected function indexDeafultParam()
    {
        return [
            'title' => '',
            'state' => '',
        ];
    }

    /**
     * @description: 审核
     * @param {*}
     * @return {*}
     */
    public function audit()
    {
        DbManager::getInstance()->invoke(function ($client) {
            $param  = $this->request()->getRequestParam();
            $errMsg = (new CommentsValidate)->validate($param, 'audit');
            if ($errMsg !== true) {
                throw new Exception($errMsg, 405);
            }
            $model = $this->model::invoke($client);
            $model->modifyData($param['id'], ['state' => $param['state']]);
            return true;
        });
        $this->afterAudit($this->request()->getRequestParam());
        Tools::json($this->response(), 200, [], '数据修改完成');
    }

    /**
     * @description: 修改显示属性后
     * @param array $param 前端参数
     * @return {*}
     */
    protected function afterAudit(array $param)
    {
        $task = TaskManager::getInstance();
        $args = [
            'func'  => 'afterAudit',
            'param' => $param
        ];
        $call = ['call' => [new CommentCache, 'del'], 'args' => $args];
        $task->async(new CacheDel($call));
        return true;
    }

    /**
     * @description: 删除后
     * @param array $param 前端参数
     * @return {*}
     */
    protected function beforeDelete(array $param)
    {
        $task = TaskManager::getInstance();
        $args = [
            'func'  => 'beforeDelete',
            'param' => $param
        ];
        $call = ['call' => [new CommentCache, 'del'], 'args' => $args];
        $task->sync(new CacheDel($call));
        return true;
    }
}