<?php

namespace App\RenderDriver;

use EasySwoole\Template\RenderInterface;
use think\Template;

class TpTemplate implements RenderInterface
{
    protected $template;
    function __construct()
    {
        $config = [
            'view_path'	=>	EASYSWOOLE_ROOT.'/App/Views/',
            'cache_path'	=>	EASYSWOOLE_ROOT.'/Temp/runtime/',
            // 'tpl_begin'	=> '{{{',
            // 'tpl_end'	=> '}}}',
            // 'taglib_begin'	=> '{{{',
            // 'taglib_end'	=> '}}}',
        ];
        $this->template = new Template($config);
    }

    public function render(string $template, ?array $data = null, ?array $options = null): ?string
    {
        ob_start();
        $this->template->assign($data);
        $this->template->fetch($template);
        $content = ob_get_contents() ;
        return $content;
    }
    public function afterRender(?string $result, string $template, array $data = [], array $options = [])
    {
    }

    public function onException(\Throwable $throwable, $arg): string
    {
        $msg = "{$throwable->getMessage()} at file:{$throwable->getFile()} line:{$throwable->getLine()}";
        trigger_error($msg);
        return $msg;
    }
}