<?php

namespace App\HttpController\Admin;

use EasySwoole\EasySwoole\Config;
use EasySwoole\EasySwoole\Task\TaskManager;
use EasySwoole\ORM\DbManager;
use EasySwoole\Template\Render;

use App\Task\CacheDel;
use App\Common\Tools;
use App\Common\Cache\ArticleCache;
use App\Model\Article\Article as ArticleModel;
use App\Common\Traits\Admin\ {
    Lists as AdminList,
    Info as AdminInfo,
    Create as AdminCreate,
    Delete as AdminDelete,
    Modify as AdminModify,
};
use App\Validate\Article as ValidateArticle;

class Article extends Base
{
    use AdminList;
    use AdminInfo;
    use AdminCreate;
    use AdminDelete;
    use AdminModify;

    /**
     * @description: 模型
     * @var Model
     */
    public $model = ArticleModel::class;

    /**
     * @description: 主键
     * @var 
     */
    public $id = 'id';
    
    /**
     * @description: 单条数据
     * @param {*}
     * @return {*}
     */
    public function info()
    {
        $data = DbManager::getInstance()->invoke(function ($client) {
            $param = $this->request()->getRequestParam();
            $id    = $param['id'] ?? 0;
            $model = $this->model::invoke($client);
            if ($id) {
                $data = $model->getInfo($id);
                $data = $data ? $data->toArray() : [];
            } else {
                $data = $model->nullData();
            }
            return $data;
        });
        $assign = $this->infoAssign();
        $assign['data'] = $data;
        $class_name = explode('\\' , static::class);
        
        $param = $this->request()->getRequestParam();
        $templateName = 'info';
        if (isset($param['contentType']) && $param['contentType'] == 1) {
            $templateName = 'markdowninfo';
        }
        $template   = '/Admin/' . end($class_name) .'/' . $templateName;
        $this->response()->write(Render::getInstance()->render($template, $assign));
    }
    

    /**
     * @description: 列表查询条件构建
     * @param array $param 前端参数
     * @return {*}
     */
    protected function indexWhere(array $param)
    {
        $where = [];
        Tools::conditionQuery($where, $param, 'name', 'name', 'like');
        Tools::conditionQuery($where, $param, 'navs_id', 'parent_id', '=');
        Tools::conditionTime($where, $param, 'stime', 'etime', 'update_time');
        Tools::conditionQuery($where, $param, 'display', 'display', '=');
        return $where;
    }

    /**
     * @description: 列表数据额外返回数据
     * @param {*}
     * @return {*}
     */
    protected function indexAssign()
    {
        return ['navs' => (new Category())->getNavTree()];
        
    }

    /**
     * @description: 列表查询条件默认字段
     * @param {*}
     * @return {*}
     */
    protected function indexDeafultParam()
    {
        return [
            'name'      => '',
            'parent_id' => '',
            'stime'     => '',
            'etime'     => '',
            'display'   => '',
        ];
    }

    /**
     * @description: 单条数据查询额外返回数据
     * @param {*}
     * @return {*}
     */
    protected function infoAssign()
    {
        $sessionName = Config::getInstance()->getConf('SESSION.name');
        $sessionId   = $this->request()->getAttribute($sessionName);
        return ['navs' => (new Category())->getNavTree(), 'mblogs' => $sessionId];   
    }

    /**
     * @description: 验证创建参数
     * @param array $param 前端参数
     * @return {*}
     */
    protected function validateCreate(array $param)
    {
        return (new ValidateArticle)->validate($param);
    }

        /**
     * @description: 创建后
     * @param array $param 前端参数
     * @return {*}
     */
    protected function afterCreate(array $param)
    {
        $task = TaskManager::getInstance();
        $args = [
            'func'  => 'afterCreate',
            'param' => $param
        ];
        $call = ['call' => [new ArticleCache, 'del'], 'args' => $args];
        $task->async(new CacheDel($call));
        return true;
    }

    /**
     * @description: 修改显示属性后
     * @param array $param 前端参数
     * @return {*}
     */
    protected function afterMofidyDisplay(array $param)
    {
        $task = TaskManager::getInstance();
        $args = [
            'func'  => 'afterMofidyDisplay',
            'param' => $param
        ];
        $call = ['call' => [new ArticleCache, 'del'], 'args' => $args];
        $task->async(new CacheDel($call));
        return true;
    }

    /**
     * @description: 删除后
     * @param array $param 前端参数
     * @return {*}
     */
    protected function beforeDelete(array $param)
    {
        $task = TaskManager::getInstance();
        $args = [
            'func'  => 'beforeDelete',
            'param' => $param
        ];
        $call = ['call' => [new ArticleCache, 'del'], 'args' => $args];
        $task->sync(new CacheDel($call));
        return true;
    }
}