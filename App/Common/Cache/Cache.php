<?php

namespace App\Common\Cache;

use EasySwoole\EasySwoole\Config;

/**
 * @description: 缓存基类
 */
class Cache
{
    /**
     * @description: 是否启用缓存
     * @var bool
     */
    public $cache;

    public function __construct()
    {
        $this->cache = Config::getInstance()->getConf('REDIS.cacheEnable');
    }
}