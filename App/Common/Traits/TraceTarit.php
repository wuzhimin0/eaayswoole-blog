<?php

namespace App\Common\Traits;

use Throwable;

/**
 * @description: 追踪类
 */
trait TraceTarit
{
    /**
     * @description: 监控错误
     * @param Callable $call 指定方法
     * @param mixed $errReturn 报错时的返回值
     * @param array $args 方法参数
     * @return {*}
     */
    public static function tryCatch(Callable $call, $errReturn, ...$args)
    {
        try {
            return call_user_func($call, ...$args);
        } catch (Throwable $e) {
            return $errReturn;
        }
    }
}