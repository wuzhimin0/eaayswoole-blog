<?php

namespace App\Common\Cache;

use App\Common\RedisCache;

/**
 * @description: 友情链接缓存
 */
class LinkCache extends Cache
{
    /**
     * @description: 删除缓存
     * @param {*}
     * @return {*}
     */    
    public function del()
    {
        if ($this->cache) {
            RedisCache::del('link:home');
        }
    }
}