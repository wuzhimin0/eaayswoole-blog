<?php

use EasySwoole\DatabaseMigrate\MigrateManager;

/**
 * filling data
 *
 * Class Setting
 */
class Setting
{
    /**
     * seeder run
     * @return void
     * @throws Throwable
     * @throws \EasySwoole\Mysqli\Exception\Exception
     */
    public function run()
    {
        // todo::connection databases and insert data
        // example
        $client = MigrateManager::getInstance()->getClient();
        $client->queryBuilder()->insertAll("setting", [
            [
                'skey'  => 'title',
                'vals'  => '',
                'descs' => '网站名称',
            ],
            [
                'skey'  => 'comment',
                'vals'  => '',
                'descs' => '开始评论',
            ],
            [
                'skey'  => 'comment_audit',
                'vals'  => '',
                'descs' => '评论自动审核通过',
            ],
            [
                'skey'  => 'theme',
                'vals'  => 'Inception',
                'descs' => '主题',
            ],
        ]);
        $client->execBuilder();
    }
}