<?php

namespace App\Common\Cache;

use EasySwoole\ORM\DbManager;

use App\Common\RedisCache;
use App\Common\Tools;
use App\Model\Article\ {
    Nav as NavModel,
    Article as ArticleModel
};

/**
 * @description: 文章分类缓存
 */
class CategoryCache extends Cache
{
/**
     * @description: 删除缓存
     * @param {*}
     * @return {*}
     */    
    public function del($args)
    {
        if ($this->cache) {
            switch ($args['func']) {
                case 'afterCreate':
                    $this->afterCreate($args['param']);
                    break;
                case 'afterMofidyDisplay':
                    $this->afterMofidyDisplay($args['param']);
                    break;
                case 'beforeDelete':
                    $this->beforeDelete($args['param']);
                    break;
            }
        }
    }

    /**
     * @description: 创建后
     * @param array $param 保存内容
     * @return {*}
     */
    protected function afterCreate($param)
    {
        if ($param['parent_id']) {
            $path = DbManager::getInstance()->invoke(function ($client) use ($param) {
                $model = NavModel::invoke($client);
                return $model->where('id', $param['parent_id'])->val('path');
            });
        } else {
            $path = '';
        }
        if (Tools::hasValue($param, 'id')) {
            $path = $path ? ($path . ':' . $param['id']) : $param['id'];
        }
        $keys = $this->calluteNavKeys($path, $param['parent_id']);
        if ($keys) {
            RedisCache::del($keys);
        }
    }

    /**
     * @description: 修改显示后
     * @param {*} $param
     * @return {*}
     */
    protected function afterMofidyDisplay($param)
    {
        $nav = DbManager::getInstance()->invoke(function ($client) use ($param) {
            $model = NavModel::invoke($client);
            return $model->where('id', $param['id'])->field('parent_id,path')->get()->toArray();
        });
        $keys = $this->calluteNavKeys($nav['path'], $nav['parent_id']);
        if ($keys) {
            RedisCache::del($keys);
        }
    }

    /**
     * @description: 删除前
     * @param {*} $param
     * @return {*}
     */
    protected function beforeDelete($param)
    {
        $keys = [];
        $ids  = explode(',', $param['id']);
        $navs = DbManager::getInstance()->invoke(function ($client) use ($ids) {
            $model = NavModel::invoke($client);
            return $model->where('id', $ids, 'in')->field('parent_id,path')->all()->toArray();
        });
        foreach ($navs as $nav) {
            $key1 = $this->calluteNavKeys($nav['path'], $nav['parent_id']);
            $keys = array_merge($keys, $key1);
        }
        if ($keys) {
            RedisCache::del($keys);
        }
    }

    /**
     * @description: 计算nav下需要删除的key
     * @param string $path 文章分类路径
     * @param int $parent_id 文章分类父级id
     * @return {*}
     */
    public function calluteNavKeys($path, $parent_id)
    {
        $keys = [];
        if ($path) {
            $paths = explode(':', $path);
            foreach ($paths as $val) {
                $keys[] = 'nav:' . $val . ':child';
                $keys[] = 'nav:' . $val . ':level';
                $pkeys  = RedisCache::keys('nav:' . $val . ':page:*');
                $keys   = array_merge($keys, $pkeys);
            }
        }
        if ($parent_id == 0) {
            $keys[] = 'nav:home';
            $keys[] = 'article:home';
        }
        return $keys;
    }
}