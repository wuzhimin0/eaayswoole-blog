<?php

namespace App\HttpController\Home;

use Exception;
use EasySwoole\ORM\DbManager;
use EasySwoole\Template\Render;
use EasySwoole\EasySwoole\Task\TaskManager;

use App\Common\Tools;
use App\Common\RedisCache;
use App\Model\Article\ {
    Nav,
    Article,
    FriendLink,
};
use App\Task\ArticleRead as TaskArticleRead;

class Index extends Base
{
    /**
     * 获取首页类
     * @return IndexInterface
     */
    public function getIndexClass()
    {
        $setting = Tools::getSetting();
        $theme = $setting['theme'] ?: 'Inception';
        return new ('App\HttpController\Home\Theme\\' . $theme . '\Index');
    }
    /**
     * @description: 首页
     * @param {*}
     * @return {*}
     */
    public function index()
    {
        $param         = $this->request()->getQueryParams();
        if (isset($param['search'])) {
            $data = $this->getIndexClass()->search($param);
        } else {
            $data = $this->getIndexClass()->index();   
        }
        $this->response()->write(Render::getInstance()->render($data['template'], $data['assign']));
    }

    /**
     * @description: 分类
     * @param {*}
     * @return {*}
     */
    public function category()
    {
        $param         = $this->request()->getQueryParams();
        if (isset($param['search'])) {
            $data = $this->getIndexClass()->search($param);
        } else {
            $data = $this->getIndexClass()->category($param);
        }
        $this->response()->write(Render::getInstance()->render($data['template'], $data['assign']));
    }

    /**
     * @description: 文章详情
     * @param {*}
     * @return {*}
     */
    public function articles()
    {
        $param         = $this->request()->getQueryParams();
        if (isset($param['search'])) {
            $data = $this->getIndexClass()->search($param);
        } else {
            $data = $this->getIndexClass()->articles($param, $this->request()); 
        }
        $this->response()->write(Render::getInstance()->render($data['template'], $data['assign']));
    }

    /**
     * @description: 搜索页
     * @param {*}
     * @return {*}
     */
    public function search()
    {
        $param  = $this->request()->getRequestParam();
        $data = $this->getIndexClass()->search($param);
        if (isset($data['redirectUrl'])) {
            $this->response()->redirect($data['redirectUrl']);
            return '';
        }
        $this->response()->write(Render::getInstance()->render($data['template'], $data['assign']));
    }
    
    /**
     * @description: 复杂json转为FormData
     * @param {*}
     * @return {*}
     */
    public function jsonToFormData()
    {
        $this->response()->write(Render::getInstance()->render('Home/Index/jsonToFormData', []));
    }
}
