<?php

namespace App\Validate;

use App\Validate\CustomRule\DateRule;

/**
 * @description: 友情链接验证
 * @param {*}
 * @return {*}
 */
class Link extends Base
{
     // 验证规则
     public $rules = [
        'name'    => 'required|mbLengthMax:30',
        'links'   => 'required|LengthMax:100|url',
        'expire'  => 'required',
        'display' => 'required|between:0,1',
    ];

    // 验证错误消息提示
    public $messages = [
        'name.required'    => '网站标题不能为空！',
        'name.mbLengthMax' => '网站标题不能超过30个字！',
        'links.required'   => '网站链接不能为空！',
        'links.LengthMax'  => '网站链接不能超过100个字',
        'links.url'        => '网站链接不是合法链接',
        'expire.required'  => '有效期不能为空！',
        'display.required' => '是否显示不能为空！',
        'display.between'  => '是否显示只能为0或1',
    ];

    // 验证字段的别名
    public $alias = [
        'name'    => '网站标题',
        'links'   => '网站链接',
        'expire'  => '有效期',
        'display' => '是否显示',
    ];
    
    /**
     * @description: 特殊规则
     * @param Validate $validate 验证器
     * @param array $data 验证数据
     * @return {*}
     */
    public function special($validate, $data)
    {
        $validate->addFunction(new DateRule(), false);
        $validate->addColumn('expire')->callUserRule(new DateRule(), '有效期不是一个有效时间');
        return $validate;
    }
}