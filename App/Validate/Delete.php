<?php

namespace App\Validate;

/**
 * @description: 删除验证
 * @param {*}
 * @return {*}
 */
class Delete extends Base
{
    // 验证规则
    public $rules = [
        'id'   => 'required',
    ];

    // 验证错误消息提示
    public $messages = [
        'id.required'  => 'id不能为空！',
    ];

    // 验证字段的别名
    public $alias = [
    ];
}