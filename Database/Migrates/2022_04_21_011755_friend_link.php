<?php

use EasySwoole\DDL\Blueprint\Create\Table as CreateTable;
use EasySwoole\DDL\Blueprint\Alter\Table as AlterTable;
use EasySwoole\DDL\Blueprint\Drop\Table as DropTable;
use EasySwoole\DDL\DDLBuilder;
use EasySwoole\DDL\Enum\Character;
use EasySwoole\DDL\Enum\Engine;

/**
 * migrate create
 * Class FriendLink
 */
class FriendLink
{
    /**
     * migrate run
     * @return string
     */
    public function up()
    {
        return DDLBuilder::create('friend_link',function (CreateTable $table){
            $table->setIfNotExists(true)->setTableComment('友情链接');          //设置表名称
            $table->setTableCharset(Character::UTF8_GENERAL_CI);     //设置表字符集
            $table->setTableEngine(Engine::INNODB);                     //设置表引擎
            $table->int('id')->setIsUnsigned()->setIsAutoIncrement()->setIsPrimaryKey()->setColumnComment('自增ID');
            $table->varchar('name', 30)->setColumnComment('网站标题');
            $table->varchar('links', 100)->setColumnComment('网站连接');
            $table->date('expire')->setColumnComment('有效期');
            $table->tinyint('display', 1)->setDefaultValue(0)->setColumnComment('0、显示 1、不显示');
        });
    }

    /**
     * migrate rollback
     * @return string
     */
    public function down()
    {
        return DDLBuilder::dropIfExists('friend_link');
    }
}
