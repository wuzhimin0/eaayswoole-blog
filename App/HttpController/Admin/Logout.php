<?php

namespace App\HttpController\Admin;

class Logout extends Base
{
     /**
     * @description: 登出
     * @param {*}
     * @return {*}
     */
    public function index()
    {
        $this->session()->del('adminUser');
        $this->response()->redirect("/admin/login.html");
    }
}