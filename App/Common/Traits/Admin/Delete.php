<?php

namespace App\Common\Traits\Admin;

use Exception;
use EasySwoole\ORM\DbManager;

use App\Common\Tools;
use App\Validate\Delete as ValidateDelete; 

/**
 * @description: 后台删除数据
 */
trait Delete
{
    /**
     * @description: 删除
     * @param {*}
     * @return {*}
     */
    public function delete()
    {
        $result = DbManager::getInstance()->invoke(function ($client) {
            $param = $this->request()->getRequestParam();
            if (($errMsg = $this->validDelete($param)) !== true) {
                throw new Exception($errMsg, 405);
            }
            $this->beforeDelete($this->request()->getRequestParam());
            $model = $this->model::invoke($client);
            $model->deleteData($param['id']);
            return true;
        });
        Tools::json($this->response(), 200, [], '数据删除完成');
    }

    /**
     * @description: 验证删除参数
     * @param array $param 前端参数
     * @return {*}
     */
    protected function validDelete(array $param)
    {
        return (new ValidateDelete)->validate($param);
    }

    /**
     * @description: 删除后
     * @param array $param 前端参数
     * @return {*}
     */
    protected function beforeDelete(array $param)
    {
        return true;
    }
}