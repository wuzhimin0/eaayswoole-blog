<?php

namespace App\Validate;

/**
 * @description: 评论验证
 */
class Comments extends Base
{
    // 验证规则
    public $rules = [
        'id'        => 'required',
        'state'     => 'required|between:1,2',
        'parent_id' => 'required|integer',
        'comment'   => 'required|mbLengthMax:1000',
    ];

    // 验证错误消息提示
    public $messages = [
        'id.required'         => 'id不能为空！',
        'state.required'      => '审核状态不能为空！',
        'state.between'       => '审核状态错误',
        'parent_id.required'  => '上级评论不能为空！',
        'parent_id.integer'   => '上级评论错误',
        'comment.required'    => '评论内容不能为空！',
        'comment.mbLengthMax' => '评论内容不能超过1000字',
    ];

    // 验证字段的别名
    public $alias = [
        'id'        => '评论id',
        'parent_id' => '上级评论',
        'comment'   => '评论内容',
        'state'     => '审核状态',
    ];

    // 验证场景
    public $sence = [
        'audit'  => ['id', 'state'],
        'create' => ['parent_id', 'comment'],
    ];
}