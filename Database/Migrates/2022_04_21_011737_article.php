<?php

use EasySwoole\DDL\Blueprint\Create\Table as CreateTable;
use EasySwoole\DDL\Blueprint\Alter\Table as AlterTable;
use EasySwoole\DDL\Blueprint\Drop\Table as DropTable;
use EasySwoole\DDL\DDLBuilder;
use EasySwoole\DDL\Enum\Character;
use EasySwoole\DDL\Enum\Engine;

/**
 * migrate create
 * Class Article
 */
class Article
{
    /**
     * migrate run
     * @return string
     */
    public function up()
    {
        return DDLBuilder::create('article',function (CreateTable $table){
            $table->setIfNotExists(true)->setTableComment('文章');          //设置表名称
            $table->setTableCharset(Character::UTF8_GENERAL_CI);     //设置表字符集
            $table->setTableEngine(Engine::INNODB);                     //设置表引擎
            $table->int('id')->setIsUnsigned()->setIsAutoIncrement()->setIsPrimaryKey()->setColumnComment('自增ID');
            $table->varchar('name', 50)->setColumnComment('标题');
            $table->longtext('content')->setIsNotNull(false)->setColumnComment('文章内容');
            $table->smallint('navs_id')->setColumnComment('文章分类id');
            $table->tinyint('display', 1)->setDefaultValue(0)->setColumnComment('0、显示 1、不显示');
            $table->varchar('keywords', 255)->setIsNotNull(false)->setColumnComment('关键词');
            $table->varchar('description', 500)->setIsNotNull(false)->setColumnComment('描述');
            $table->int('read_number')->setDefaultValue(0)->setColumnComment('阅读数');
            $table->date('update_time')->setIsNotNull(false)->setColumnComment('创建时间');
            $table->fulltext('article_fullindex', ['name', 'content', 'keywords', 'description']);
        });
    }

    /**
     * migrate rollback
     * @return string
     */
    public function down()
    {
        return DDLBuilder::dropIfExists('article');
    }
}
