<?php

namespace App\HttpController\Admin;

use App\Common\RedisCache;
use EasySwoole\ORM\DbManager;
use EasySwoole\Template\Render;

use App\Common\Tools;
use App\Model\Setting\Setting as AdminSetting;

class Setting extends Base
{

    /**
     * @description: 模型
     * @var Model
     */
    public $model = AdminSetting::class;

    /**
     * @description: 主键
     * @var 
     */
    public $id = 'id';

    /**
     * @description: 单条数据
     * @param {*}
     * @return {*}
     */
    public function info()
    {
        $data = DbManager::getInstance()->invoke(function ($client) {
            $model = $this->model::invoke($client);
            $data  = $model->all()->toArray();
            return $data;
        });
        $assign['data'] = $data;
        $template       = '/Admin/Setting/info';
        $this->response()->write(Render::getInstance()->render($template, $assign));
    }

    /**
     * @description: 创建
     * @param {*}
     * @return {*}
     */
    public function create()
    {
        $param = $this->request()->getRequestParam();
        $data  = [];
        foreach ($param['data'] as $key => $val) {
            $data[] = [
                'id'   => $key,
                'vals' => $val['vals'],
            ];
        }
        $sqls = Tools::batchUpdateSql($data, 'setting');
        foreach ($sqls as $sql) {
            $this->model::create()->func(function ($builder) use ($sql){
                $builder->raw($sql);
                return true;
            });
        }
        RedisCache::clean();
        Tools::setSetting();
        Tools::json($this->response(), 200, [], '数据修改完成');
    }
}
