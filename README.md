# easyswoole+think-template+mysql+redis的博客系统
#### 安装
* 依赖安装
> composer install
* 框架安装
> * php vendor/bin/easyswoole install
> * 安装时提示输入N，不释放Index.php、Router.php 和 App 目录在项目根目录
* 配置dev、produce文件
> REDIS.cacheEnable=true代表启用redis缓存
* 初始化数据库
> php easyswoole migrate run
> php easyswoole migrate seed

#### mysql
> mysql文章内容配置全文索引

#### 预览
* 首页![首页](./readme/home.png)
* 分类页![分类页](./readme/category.png)
* 文章页![文章页](./readme/article.png)
* 后台管理![后台管理](./readme/admin.png)
* 评论管理![评论管理](./readme/comment.png)
    评论可选择开启