<?php

namespace App\Common;

use EasySwoole\Component\Singleton;

class Session extends \EasySwoole\Session\Session
{
    use Singleton;
}