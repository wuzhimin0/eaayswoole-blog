<?php


namespace App\HttpController;

use App\Common\Tools;
use EasySwoole\Http\AbstractInterface\AbstractRouter;
use FastRoute\RouteCollector;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;
use EasySwoole\Template\Render;

class Router extends AbstractRouter
{
    function initialize(RouteCollector $routeCollector)
    {
        $this->setGlobalMode(true);
        $this->parseParams(static::PARSE_PARAMS_IN_GET);
        $routeCollector->addGroup('/admin', function (RouteCollector $collector) {
            $collector->addRoute('GET', '[.html]', 'Admin/Index/index');
            $collector->addRoute('GET', '/zong[.html]', 'Admin/Index/zong');
            $collector->addRoute('POST', '/upload[.html]', 'Admin/Upload/index');
            $collector->addRoute('GET', '/login[.html]', function (Request $request, Response $response) {
                $response->write(Render::getInstance()->render('Admin/Login/index', []));
            });
            $collector->addRoute('POST', '/loginDo[.html]', 'Admin/Login/Login');
            $collector->addRoute('GET', '/logout[.html]', 'Admin/Logout/index');
            $collector->addGroup('/category', function (RouteCollector $collector) {
                $collector->addRoute('GET', '', 'Admin/Category/index');
                $collector->addRoute('GET', '/index[.html]', 'Admin/Category/index');
                $collector->addRoute('GET', '/info[.html]', 'Admin/Category/info');
                $collector->addRoute('POST', '/create[.html]', 'Admin/Category/create');
                $collector->addRoute('POST', '/delete[.html]', 'Admin/Category/delete');
                $collector->addRoute('POST', '/display[.html]', 'Admin/Category/mofidyDisplay');
            });
            $collector->addGroup('/article', function (RouteCollector $collector) {
                $collector->addRoute('GET', '', 'Admin/Article/index');
                $collector->addRoute('GET', '/index[.html]', 'Admin/Article/index');
                $collector->addRoute('GET', '/info[.html]', 'Admin/Article/info');
                $collector->addRoute('POST', '/create[.html]', 'Admin/Article/create');
                $collector->addRoute('POST', '/delete[.html]', 'Admin/Article/delete');
                $collector->addRoute('POST', '/display[.html]', 'Admin/Article/mofidyDisplay');
            });
            $collector->addGroup('/link', function (RouteCollector $collector) {
                $collector->addRoute('GET', '', 'Admin/Link/index');
                $collector->addRoute('GET', '/index[.html]', 'Admin/Link/index');
                $collector->addRoute('GET', '/info[.html]', 'Admin/Link/info');
                $collector->addRoute('POST', '/create[.html]', 'Admin/Link/create');
                $collector->addRoute('POST', '/delete[.html]', 'Admin/Link/delete');
                $collector->addRoute('POST', '/display[.html]', 'Admin/Link/mofidyDisplay');
            });
            $collector->addGroup('/user', function (RouteCollector $collector) {
                $collector->addRoute('GET', '', 'Admin/User/index');
                $collector->addRoute('GET', '/index[.html]', 'Admin/User/index');
                $collector->addRoute('GET', '/info[.html]', 'Admin/User/info');
                $collector->addRoute('POST', '/create[.html]', 'Admin/User/create');
                $collector->addRoute('POST', '/delete[.html]', 'Admin/User/delete');
            });
            $collector->addGroup('/setting', function (RouteCollector $collector) {
                $collector->addRoute('GET', '/info[.html]', 'Admin/Setting/info');
                $collector->addRoute('POST', '/create[.html]', 'Admin/Setting/create');
            });
            $collector->addGroup('/comments', function (RouteCollector $collector) {
                $collector->addRoute('GET', '', 'Admin/Comments/index');
                $collector->addRoute('GET', '/index[.html]', 'Admin/Comments/index');
                $collector->addRoute('POST', '/audit[.html]', 'Admin/Comments/audit');
                $collector->addRoute('POST', '/delete[.html]', 'Admin/Comments/delete');
            });
        });
        $routeCollector->addRoute('GET', '/', 'Home/Index/index');
        $routeCollector->addRoute('GET', '/index[.html]', 'Home/Index/index');
        $routeCollector->addRoute('GET', '/category/{id:\d+}[.html]', 'Home/Index/category');
        $routeCollector->addRoute('GET', '/articles/{id:\d+}[.html]', 'Home/Index/articles');
        $routeCollector->addRoute(['GET', 'POST'], '/search[.html]', 'Home/Index/search');
        $routeCollector->addRoute('POST', '/comment[.html]', 'Home/Comment/comment');
        $routeCollector->addRoute('POST', '/comments[.html]', 'Home/Comment/index');
        $routeCollector->addRoute('GET', '/jsonToFormData', 'Home/Index/jsonToFormData');

        $this->setMethodNotAllowCallBack(function (Request $request, Response $response){
            $response->withStatus(404);
            $response->write(Render::getInstance()->render('Public/404', []));
            return false;
        });
        $this->setRouterNotFoundCallBack(function (Request $request, Response $response){
            $response->withStatus(404);
            $response->write('123');
            // $response->write(Render::getInstance()->render('Public/404', []));
            return false;
        });
    }
}
