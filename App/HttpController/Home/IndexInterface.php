<?php

namespace App\HttpController\Home;

interface IndexInterface
{
    /**
     * 首页
     */
    public function index();
    /**
     * 分类
     */
    public function category($param);
    /**
     * 详情
     */
    public function articles($param, $request);
    /**
     * 搜索
     */
    public function search($param);
}