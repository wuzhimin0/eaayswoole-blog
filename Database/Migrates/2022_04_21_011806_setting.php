<?php

use EasySwoole\DDL\Blueprint\Create\Table as CreateTable;
use EasySwoole\DDL\Blueprint\Alter\Table as AlterTable;
use EasySwoole\DDL\Blueprint\Drop\Table as DropTable;
use EasySwoole\DDL\DDLBuilder;
use EasySwoole\DDL\Enum\Character;
use EasySwoole\DDL\Enum\Engine;

/**
 * migrate create
 * Class Setting
 */
class Setting
{
    /**
     * migrate run
     * @return string
     */
    public function up()
    {
        return DDLBuilder::create('setting',function (CreateTable $table){
            $table->setIfNotExists(true)->setTableComment('系统设置');          //设置表名称
            $table->setTableCharset(Character::UTF8_GENERAL_CI);     //设置表字符集
            $table->setTableEngine(Engine::INNODB);                     //设置表引擎
            $table->tinyint('id')->setIsUnsigned()->setIsAutoIncrement()->setIsPrimaryKey()->setColumnComment('自增ID');
            $table->varchar('skey', 100)->setColumnComment('设置字段名');
            $table->varchar('vals', 255)->setIsNotNull(false)->setColumnComment('设置值');
            $table->varchar('descs', 255)->setColumnComment('描述');
        });
    }

    /**
     * migrate rollback
     * @return string
     */
    public function down()
    {
        return DDLBuilder::dropIfExists('setting');
    }
}
