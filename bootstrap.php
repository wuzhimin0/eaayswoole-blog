<?php

use EasySwoole\EasySwoole\Core;
use EasySwoole\EasySwoole\Config as EasySwooleConfig;
use EasySwoole\Command\CommandManager;
use EasySwoole\DatabaseMigrate\ {
    MigrateManager,
    Config\Config,
    MigrateCommand,
};

//全局bootstrap事件
date_default_timezone_set('Asia/Shanghai');

CommandManager::getInstance()->addCommand(new MigrateCommand());
Core::getInstance()->initialize();
$dbconfig = EasySwooleConfig::getInstance()->getConf('MYSQL');
$config = new Config();
$config->setHost($dbconfig['host']);
$config->setPort($dbconfig['port']);
$config->setUser($dbconfig['user']);
$config->setPassword($dbconfig['password']);
$config->setDatabase($dbconfig['database']);
$config->setTimeout($dbconfig['timeout']);
$config->setCharset($dbconfig['charset']);
// 迁移文件目录的绝对路径
$config->setMigratePath(EASYSWOOLE_ROOT . '/Database/Migrates/');
// 数据填充目录绝对路径
$config->setSeederPath(EASYSWOOLE_ROOT . '/Database/Seeds/');
MigrateManager::getInstance($config);