<?php

namespace App\Model\Article;

use App\Common\Tools;
use EasySwoole\ORM\ {
    AbstractModel,
    Utility\Schema\Table,
};
use EasySwoole\Mysqli\QueryBuilder;

use App\Model\Base;

/**
 * 文章评论
 * Class Comments
 */
class Comments extends Base
{

    protected $tableName = 'article_comments';

    /**
     * 表的字段
     * @return Table
     */
    public function schemaInfo(bool $isCache = true): Table
    {
        $table = new Table($this->tableName);
        $table->colInt('id')->setIsPrimaryKey(true);
        $table->colInt('article_id');
        $table->colInt('parent_id');
        $table->colVarChar('comment', 1000);
        $table->colVarChar('ip', 100);
        $table->colTinyInt('state', 1);
        $table->colDateTime('comment_time', 255);
        /* create table article_comments (
            id int(11) unsigned auto_increment,
            article_id int(11) comment '文章id',
            parent_id int(11) unsigned default 0 comment '上级评论id',
            comment varchar(1000) comment '评论内容',
            ip varchar(100) comment '评论者ip',
            state tinyint(1) default 0 comment '0、未审核 1、审核未通过 2、审核通过',
            comment_time datetime comment '评论时间',
            primary key(id)
        ) engine = innodb comment '文章评论';
        */
        return $table;
    }

    /**
     * @description: 关联文章
     * @param {*}
     * @return {*}
     */
    public function article()
    {
        return $this->hasOne(Article::class, function (QueryBuilder $query) {
            $query->fields(['id', 'name']);
        }, 'article_id', 'id');
    }

    /**
     * $value mixed 是原值
     * $data  array 是当前model所有的值 
     */
    protected function getStateTextAttr($value, $data)
    {
        $states = [0 => '未审核', 1 => '审核未通过', 2 => '审核已通过'];
        return $states[$data['state']] ?? '';
    }

    /**
     * @description: 分页列表连贯查询
     * @param {*}
     * @return {*}
     */    
    protected function adminPagesCoherent()
    {
        $this->with(['article'])->order('comment_time', 'desc');
        return $this;
    }

     /**
     * @description: 分页列表数据处理
     * @param {*}
     * @return {*}
     */    
    protected function adminPagesData($list)
    {
        $datas = $list->toArray();
        foreach ($list as $key => $val) {
            $datas[$key]['title']      = $val->article->name;
            $datas[$key]['state_text'] = $val->StateText;
        }
        return $datas;
    }

    /**
     * @description: 拼接创建参数
     * @param {*}
     * @return {*}
     */
    protected function createDatas()
    {
        return [
            'article_id'   => $this->param['article_id'],
            'parent_id'    => $this->param['parent_id'],
            'comment'      => $this->param['comment'],
            'ip'           => $this->param['ip'],
            'comment_time' => date('Y-m-d H:i:s'),
            'state'        => Tools::getSetting()['comment_audit'] == '是' ? 2 : 0,
        ];
    }

    /**
     * @description: 列表
     * @param {*}
     * @return {*}
     */    
    public function list($page, $limit, $where)
    {
        return static::tryCatch(function($page, $limit, $where) {
            $model = $this->where($where)->order('comment_time', 'desc')
                    ->limit($limit * ($page - 1), $limit)->withTotalCount();

            $list   = $model->all(null);
            $result = $model->lastQueryResult();
            $total  = $result->getTotalCount();
            $datas  = $list->toArray();

            return [$datas, $total];
        }, [[], 0], $page, $limit, $where);
    }
}