<?php

namespace App\Model;

use EasySwoole\ORM\ {
    AbstractModel,
};
use App\Common\Traits\TraceTarit;

class Base extends AbstractModel
{
    use TraceTarit;

    /**
     * @description: 前端参数
     * @var array
     */
    public $param;

    /**
     * @description: 列表
     * @param {*}
     * @return {*}
     */    
    public function adminPages($page, $limit, $where)
    {
        return static::tryCatch(function($page, $limit, $where) {
            $model = $this->adminPagesCoherent()->where($where)
                    ->limit($limit * ($page - 1), $limit)->withTotalCount();

            $list   = $model->all(null);
            $result = $model->lastQueryResult();
            $total  = $result->getTotalCount();
            $datas  = $this->adminPagesData($list);

            return [$datas, $total];
        }, [[], 0], $page, $limit, $where);
    }

    /**
     * @description: 分页列表连贯查询
     * @param {*}
     * @return {*}
     */    
    protected function adminPagesCoherent()
    {
        return $this;
    }

    /**
     * @description: 分页列表数据处理
     * @param {*}
     * @return {*}
     */    
    protected function adminPagesData($list)
    {
        return $list->toArray();
    }

    /**
     * @description: 获取单条数据
     * @param int $id 主键
     * @return {*}
     */    
    public function getInfo($id)
    {
        return static::tryCatch(function($id) {
            return $this->get($id);
        }, [], $id);
    }
  
    /**
     * @description: 创建数据
     * @param array $param 前端数据
     * @param AbstractModel $model 模型
     * @return {*}
     */
    public function createData($param, $model = false)
    {
        $this->param = $param;
        return static::tryCatch(function($model) {
            $data = $this->createDatas();
            
            if ($model && $model instanceof AbstractModel) {
                $model->update($data);
            } else {
                $model = static::create($data);
                $model->id = $model->save();
            }
            $this->createAfter($model);
            return true;
        }, false, $model);
        
    }

    /**
     * @description: 拼接创建参数
     * @param {*}
     * @return {*}
     */
    protected function createDatas()
    {
        return [];
    }

    /**
     * @description: 创建后处理
     * @param AbstractModel $model
     * @return {*}
     */
    protected function createAfter($model)
    {}

    /**
     * @description: 删除数据
     * @param array|int $ids
     * @return {*}
     */    
    public function deleteData($ids)
    {
        $ids = is_string($ids) ? explode(',', $ids) : $ids;
        return static::tryCatch(function($ids) {
            return $this->destroy($ids);
        }, 0, $ids);
    }

    /**
     * @description: 修改数据属性
     * @param array|int $ids
     * @param array $data
     * @return {*}
     */    
    public function modifyData($ids, $data)
    {
        $ids = explode(',', $ids);
        return static::tryCatch(function($ids, $data) {
            return $this->update($data, [
                'id' => [$ids, 'in']
            ]);
        }, 0, $ids, $data);
    }

    /**
     * @description: 范围查询条件
     * @param {*}
     * @return {*}
     */
    public function scopWhere()
    {
        return $this;
    }
}