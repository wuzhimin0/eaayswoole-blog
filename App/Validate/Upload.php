<?php

namespace App\Validate;

/**
 * @description: 上传验证
 * @param {*}
 * @return {*}
 */
class Upload extends Base
{
    // 验证规则
    public $rules = [
        'file'   => 'required',
    ];

    // 验证错误消息提示
    public $messages = [
        'name.required'  => '文件不能为空！',
        'file.allowFile' => '文件类型错误',
    ];

    // 验证字段的别名
    public $alias = [
        'file'   => '文件',
    ];

    /**
     * @description: 特殊规则
     * @param Validate $validate 验证器
     * @param array $data 验证数据
     * @return {*}
     */
    public function special($validate, $data)
    {
        $validate->addColumn('file')->allowFile(['jpg', 'jpeg', 'png', 'gif', 'doc', 'docx', 'xlsx', 'xls', 'pdf']);
        return $validate;
    }
}