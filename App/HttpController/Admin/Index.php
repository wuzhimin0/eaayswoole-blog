<?php

namespace App\HttpController\Admin;

use App\Common\Tools;
use EasySwoole\ORM\DbManager;
use EasySwoole\Mysqli\QueryBuilder;
use EasySwoole\Template\Render;
use EasySwoole\EasySwoole\SysConst;

use App\Model\Article\ {
    Nav,
    Article,
    ArticleRead,
};
use Exception;

class Index extends Base
{
    /**
     * @description: 首页
     * @param {*}
     * @return {*}
     */
    public function index()
    {
        $user   = $this->session()->get('adminUser');
        $assign = ['user' => $user];
        $this->response()->write(Render::getInstance()->render('Admin/Index/index', $assign));
    }
    /**
     * @description: 看板
     * @param {*}
     * @return {*}
     */
    public function zong()
    {
        $queryBuild = new QueryBuilder();
        $queryBuild->raw('select version()');
        $mysql = DbManager::getInstance()->query($queryBuild, true);

        $result = DbManager::getInstance()->invoke(function ($client){
            $navModel  = Nav::invoke($client);
            $artModel  = Article::invoke($client);
            $readModel = ArticleRead::invoke($client);
            
            $date    = date('Y-m-d');
            $nav     = $navModel->count();
            $art     = $artModel->count();
            $allread = $artModel->sum('read_number');
            $dayread = $readModel->where('read_time', [$date . ' 00:00:00', $date . ' 23:59:59'], 'between')
                        ->count();
            return [$nav, $art, $allread, $dayread];
        });
        $user   = $this->session()->get('adminUser');
        $assign = [
            'user'       => $user,
            'easyswoole' => SysConst::EASYSWOOLE_VERSION,
            'php'        => phpversion(),
            'swoole'     => phpversion('swoole'),
            'mysql'      => $mysql->getResult()[0]['version()'],
            'os'         => PHP_OS,
            'nav'        => $result[0],
            'art'        => $result[1],
            'allread'    => $result[2],
            'dayread'    => $result[3],
        ];
        $this->response()->write(Render::getInstance()->render('Admin/Index/zong', $assign));
    }
}
