<?php

namespace App\Model\Article;

use EasySwoole\ORM\ {
    AbstractModel,
    Utility\Schema\Table,
};
use App\Model\Base;

/**
 * 友情链接
 * Class FriendLink
 */
class FriendLink extends Base
{

    protected $tableName = 'friend_link';

    /**
     * 表的字段
     * @return Table
     */
    public function schemaInfo(bool $isCache = true): Table
    {
        $table = new Table($this->tableName);
        $table->colSmallInt('id')->setIsPrimaryKey(true);
        $table->colVarChar('name', 30);
        $table->colVarChar('links', 100);
        $table->colDate('expire');
        $table->colTinyInt('display');
        /* alter table friend_link add expire date comment '有效期',
        modify id smallint unsigned auto_increment,  
        modify display tinyint(1) default 0 comment '0、显示 1、不显示' ;
        */
        return $table;
    }

    /**
     * @description: 范围查询条件
     * @param {*}
     * @return {*}
     */
    public function scopWhere()
    {
        $this->where('display', 0);
        return $this;
    }

    /**
     * @description: 获取未到期的友情链接
     * @param {*}
     * @return {*}
     */
    public function getLinks()
    {
        return static::tryCatch(function() {
            $where = ['expire' => [date('Y-m-d'), '>=']];
            return $this->scopWhere()->where($where)->all()->toArray();
        }, [], []);  
    }

    /**
     * @description: 拼接创建参数
     * @param {*}
     * @return {*}
     */
    protected function createDatas()
    {
        return [
            'name'    => $this->param['name'],
            'links'   => $this->param['links'],
            'display' => $this->param['display'],
            'expire'  => $this->param['expire'],
        ];
    }
    
    /**
     * @description: 空数据
     * @param {*}
     * @return {*}
     */
    public function nullData()
    {
        return [
            'id'      => '',
            'name'    => '',
            'links'   => '',
            'display' => '',
            'expire'  => '',
        ];
    }
}