<?php

namespace App\Common\Traits\Admin;

use EasySwoole\ORM\DbManager;
use EasySwoole\Template\Render;

/**
 * @description: 后台单条数据
 */
trait Info
{
    /**
     * @description: 单条数据
     * @param {*}
     * @return {*}
     */
    public function info()
    {
        $data = DbManager::getInstance()->invoke(function ($client) {
            $param = $this->request()->getRequestParam();
            $id    = $param['id'] ?? 0;
            $model = $this->model::invoke($client);
            if ($id) {
                $data = $model->getInfo($id);
                $data = $data ? $data->toArray() : [];
            } else {
                $data = $model->nullData();
            }
            return $data;
        });
        $assign = $this->infoAssign();
        $assign['data'] = $data;
        $class_name = explode('\\' , static::class);
        $template   = '/Admin/' . end($class_name) .'/info';
        $this->response()->write(Render::getInstance()->render($template, $assign));
    }

    /**
     * @description: 单条数据查询额外返回数据
     * @param {*}
     * @return {*}
     */
    protected function infoAssign()
    {
        return [];
    }
}