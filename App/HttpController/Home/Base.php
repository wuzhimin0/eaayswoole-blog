<?php

namespace App\HttpController\Home;


use EasySwoole\Http\AbstractInterface\Controller;
use EasySwoole\Template\Render;

class Base extends Controller
{
    protected function actionNotFound(?string $action)
    {
        $this->response()->withStatus(404);
        $this->response()->write(Render::getInstance()->render('Public/404', []));
    }
}
