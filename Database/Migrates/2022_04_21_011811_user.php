<?php

use EasySwoole\DDL\Blueprint\Create\Table as CreateTable;
use EasySwoole\DDL\Blueprint\Alter\Table as AlterTable;
use EasySwoole\DDL\Blueprint\Drop\Table as DropTable;
use EasySwoole\DDL\DDLBuilder;
use EasySwoole\DDL\Enum\Character;
use EasySwoole\DDL\Enum\Engine;

/**
 * migrate create
 * Class User
 */
class User
{
    /**
     * migrate run
     * @return string
     */
    public function up()
    {
        return DDLBuilder::create('auth_user',function (CreateTable $table){
            $table->setIfNotExists(true)->setTableComment('用户');          //设置表名称
            $table->setTableCharset(Character::UTF8_GENERAL_CI);     //设置表字符集
            $table->setTableEngine(Engine::INNODB);                     //设置表引擎
            $table->tinyint('id')->setIsUnsigned()->setIsAutoIncrement()->setIsPrimaryKey()->setColumnComment('自增ID');
            $table->varchar('username', 100)->setColumnComment('登录名');
            $table->varchar('password', 255)->setColumnComment('密码，password_hash方法');
            $table->datetime('last_login')->setIsNotNull(false)->setColumnComment('最后登录时间');
            $table->varchar('last_logip', 50)->setIsNotNull(false)->setColumnComment('最后登录ip');
            $table->tinyint('is_delete', 1)->setDefaultValue(0)->setColumnComment('软删除 0、未删除 1、已删');
        });
    }

    /**
     * migrate rollback
     * @return string
     */
    public function down()
    {
        return DDLBuilder::dropIfExists('auth_user');
    }
}
