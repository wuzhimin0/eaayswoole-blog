<?php

use EasySwoole\DDL\Blueprint\Create\Table as CreateTable;
use EasySwoole\DDL\Blueprint\Alter\Table as AlterTable;
use EasySwoole\DDL\Blueprint\Drop\Table as DropTable;
use EasySwoole\DDL\DDLBuilder;
use EasySwoole\DDL\Enum\Character;
use EasySwoole\DDL\Enum\Engine;

/**
 * migrate create
 * Class ArticleComments
 */
class ArticleComments
{
    /**
     * migrate run
     * @return string
     */
    public function up()
    {
        return DDLBuilder::create('article_comments',function (CreateTable $table){
            $table->setIfNotExists(true)->setTableComment('文章评论');          //设置表名称
            $table->setTableCharset(Character::UTF8_GENERAL_CI);     //设置表字符集
            $table->setTableEngine(Engine::INNODB);                     //设置表引擎
            $table->int('id')->setIsUnsigned()->setIsAutoIncrement()->setIsPrimaryKey()->setColumnComment('自增ID');
            $table->int('article_id')->setIsUnsigned()->setColumnComment('文章id');
            $table->int('parent_id')->setIsUnsigned()->setDefaultValue(0)->setColumnComment('上级评论id');
            $table->varchar('comment', 1000)->setColumnComment('评论内容');
            $table->varchar('ip', 100)->setColumnComment('评论者ip');
            $table->tinyint('state')->setDefaultValue(0)->setColumnComment('0、未审核 1、审核未通过 2、审核通过');
            $table->datetime('comment_time')->setColumnComment('评论时间');
        });
    }

    /**
     * migrate rollback
     * @return string
     */
    public function down()
    {
        return DDLBuilder::dropIfExists('article_comments');
    }
}
