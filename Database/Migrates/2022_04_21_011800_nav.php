<?php

use EasySwoole\DDL\Blueprint\Create\Table as CreateTable;
use EasySwoole\DDL\Blueprint\Alter\Table as AlterTable;
use EasySwoole\DDL\Blueprint\Drop\Table as DropTable;
use EasySwoole\DDL\DDLBuilder;
use EasySwoole\DDL\Enum\Character;
use EasySwoole\DDL\Enum\Engine;

/**
 * migrate create
 * Class Nav
 */
class Nav
{
    /**
     * migrate run
     * @return string
     */
    public function up()
    {
        return DDLBuilder::create('nav',function (CreateTable $table){
            $table->setIfNotExists(true)->setTableComment('文章分类');          //设置表名称
            $table->setTableCharset(Character::UTF8_GENERAL_CI);     //设置表字符集
            $table->setTableEngine(Engine::INNODB);                     //设置表引擎
            $table->smallint('id')->setIsUnsigned()->setIsAutoIncrement()->setIsPrimaryKey()->setColumnComment('自增ID');
            $table->varchar('name', 20)->setColumnComment('分类名称');
            $table->varchar('path', 255)->setIsNotNull(false)->setColumnComment('路径');
            $table->smallint('parent_id')->setDefaultValue(0)->setIsUnsigned()->setColumnComment('上级id');
            $table->tinyint('display', 1)->setDefaultValue(0)->setColumnComment('0、显示 1、不显示');
            $table->tinyint('is_show', 1)->setDefaultValue(0)->setColumnComment('0、不展示在首页 1、展示在首页');
        });
    }

    /**
     * migrate rollback
     * @return string
     */
    public function down()
    {
        return DDLBuilder::dropIfExists('nav');
    }
}
