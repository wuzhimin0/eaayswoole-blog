<?php

use EasySwoole\DDL\Blueprint\Create\Table as CreateTable;
use EasySwoole\DDL\Blueprint\Alter\Table as AlterTable;
use EasySwoole\DDL\Blueprint\Drop\Table as DropTable;
use EasySwoole\DDL\DDLBuilder;
use EasySwoole\DDL\Enum\Character;
use EasySwoole\DDL\Enum\Engine;

/**
 * migrate create
 * Class ArticleRead
 */
class ArticleRead
{
    /**
     * migrate run
     * @return string
     */
    public function up()
    {
        return DDLBuilder::create('article_read',function (CreateTable $table){
            $table->setIfNotExists(true)->setTableComment('文章阅读记录');          //设置表名称
            $table->setTableCharset(Character::UTF8_GENERAL_CI);     //设置表字符集
            $table->setTableEngine(Engine::INNODB);                     //设置表引擎
            $table->int('id')->setIsUnsigned()->setIsAutoIncrement()->setIsPrimaryKey()->setColumnComment('自增ID');
            $table->smallint('navs_id')->setColumnComment('文章分类id');
            $table->int('article_id')->setIsUnsigned()->setColumnComment('文章id');
            $table->varchar('ip', 100)->setColumnComment('阅读ip');
            $table->datetime('read_time')->setColumnComment('阅读时间');
        });
    }

    /**
     * migrate rollback
     * @return string
     */
    public function down()
    {
        return DDLBuilder::dropIfExists('article_read');
    }
}
