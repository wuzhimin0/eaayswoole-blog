<?php

namespace App\Validate;

/**
 * @description: 文章分类验证
 * @param {*}
 * @return {*}
 */
class Nav extends Base
{
    // 验证规则
    public $rules = [
        'name'      => 'required|mbLengthMax:20',
        'parent_id' => 'required|integer',
        'display'   => 'required|between:0,1',
        'is_show'   => 'required|between:0,1',
    ];

    // 验证错误消息提示
    public $messages = [
        'name.required'      => '分类标题不能为空！',
        'name.mbLengthMax'   => '分类标题不能超过20字！',
        'parent_id.required' => '父级分类不能为空！',
        'parent_id.integer'  => '父级分类错误',
        'display.required'   => '是否显示不能为空！',
        'display.between'    => '是否显示只能为0或1',
        'is_show.required'   => '是否展示在首页不能为空！',
        'is_show.between'    => '是否展示在首页只能为0或1',
    ];

    // 验证字段的别名
    public $alias = [
        'name'      => '分类标题',
        'parent_id' => '父级分类',
        'display'   => '是否显示',
        'is_show'   => '是否展示在首页',
    ];
}