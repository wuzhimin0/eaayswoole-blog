<?php

namespace App\Task;

use EasySwoole\ORM\DbManager;
use EasySwoole\Mysqli\QueryBuilder;
use EasySwoole\Task\AbstractInterface\TaskInterface;

use App\Model\Article\ {
    Article,
    ArticleRead as ModelArticleRead
};

/**
 * @description: 阅读文章
 */
class ArticleRead implements TaskInterface
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function run(int $taskId, int $workerIndex)
    {
        $result = DbManager::getInstance()->invoke(function ($client) {
            $articleModel = Article::invoke($client);
            $readeModel   = ModelArticleRead::invoke($client);
            $articleModel->modifyData($this->data['id'], [
                'read_number' => QueryBuilder::inc(1),
            ]);
            $readeModel->createData([
                'article_id' => $this->data['id'], 'navs_id' => $this->data['navs_id'], 
                'ip' => $this->data['ip']
            ], false);
        });
    }

    public function onException(\Throwable $throwable, int $taskId, int $workerIndex)
    {
        // 异常处理
    }
}