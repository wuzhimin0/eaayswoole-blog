<?php

namespace App\Validate;

/**
 * @description: 文章验证
 * @param {*}
 * @return {*}
 */
class Article extends Base
{
    // 验证规则
    public $rules = [
        'navs_id'     => 'required|integer',
        'name'        => 'required|mbLengthMax:50',
        'keywords'    => 'mbLengthMax:255',
        'description' => 'mbLengthMax:500',
        'display'     => 'required|between:0,1',
    ];

    // 验证错误消息提示
    public $messages = [
        'navs_id.required'        => '分类不能为空！',
        'navs_id.integer'         => '分类错误',
        'name.required'           => '标题不能为空！',
        'name.mbLengthMax'        => '标题不能超过50个字！',
        'keywords.mbLengthMax'    => '关键词不能超过255个字',
        'description.mbLengthMax' => '描述不能超过500个字',
        'display.required'        => '是否显示不能为空！',
        'display.between'         => '是否显示只能为0或1',
    ];

    // 验证字段的别名
    public $alias = [
        'navs_id'     => '分类',
        'name'        => '标题',
        'keywords'    => '关键词',
        'description' => '描述',
        'display'     => '是否显示',
    ];
}