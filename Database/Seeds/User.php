<?php

use EasySwoole\DatabaseMigrate\MigrateManager;

/**
 * filling data
 *
 * Class User
 */
class User
{
    /**
     * seeder run
     * @return void
     * @throws Throwable
     * @throws \EasySwoole\Mysqli\Exception\Exception
     */
    public function run()
    {
        // todo::connection databases and insert data
        // example
        $client = MigrateManager::getInstance()->getClient();
        $client->queryBuilder()->insert('auth_user', [
            'username'  => 'admin',
            'password'  =>  password_hash('123321', PASSWORD_DEFAULT),
            'is_delete' => 0,
        ]);
        $client->execBuilder();
    }
}
