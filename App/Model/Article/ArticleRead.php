<?php

namespace App\Model\Article;

use EasySwoole\Mysqli\QueryBuilder;
use EasySwoole\ORM\ {
    AbstractModel,
    Utility\Schema\Table,
};
use App\Model\Base;

/**
 * 文章阅读记录
 * Class ArticleRead
 */
class ArticleRead extends Base
{
    protected $tableName = 'article_read';

    /**
     * 表的字段
     * 此处需要返回一个 EasySwoole\ORM\Utility\Schema\Table
     * @return Table
     */
    public function schemaInfo(bool $isCache = true): Table
    {
        $table = new Table($this->tableName);
        $table->colInt('id')->setIsPrimaryKey(true);
        $table->colInt('article_id');
        $table->colInt('navs_id');
        $table->colVarChar('ip', 100);
        $table->colDateTime('read_time', 255);
        /* create table article_read (
            id int(11) unsigned auto_increment,
            navs_id int(11) comment '文章分类id',
            article_id int(11) comment '文章id',
            read_time datetime comment '阅读时间',
            ip varchar(100) comment '阅读ip',
            primary key(id)
        ) engine = innodb comment '文章阅读记录';
        */
        return $table;
    }

    /**
     * @description: 拼接创建参数
     * @param {*}
     * @return {*}
     */
    protected function createDatas()
    {
        return [
            'navs_id'     => $this->param['navs_id'],
            'article_id'  => $this->param['article_id'],
            'read_time'   => date('Y-m-d H:i:s'),
            'ip'          => $this->param['ip'],
        ];
    }
}