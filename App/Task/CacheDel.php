<?php

namespace App\Task;

use EasySwoole\ORM\DbManager;
use EasySwoole\Mysqli\QueryBuilder;
use EasySwoole\Task\AbstractInterface\TaskInterface;

use App\Model\Article\ {
    Article,
    ArticleRead as ModelArticleRead
};

/**
 * @description: 删除缓存
 */
class CacheDel implements TaskInterface
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function run(int $taskId, int $workerIndex)
    {
        call_user_func($this->data['call'], $this->data['args']);
    }

    public function onException(\Throwable $throwable, int $taskId, int $workerIndex)
    {
        // 异常处理
    }
}