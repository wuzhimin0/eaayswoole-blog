<?php

namespace App\Common\Traits\Admin;

use Exception;
use EasySwoole\ORM\DbManager;

use App\Common\Tools;

/**
 * @description: 后台修改数据
 */
trait Modify
{
    /**
     * @description: 修改显示属性
     * @param {*}
     * @return {*}
     */
    public function mofidyDisplay()
    {
        DbManager::getInstance()->invoke(function ($client) {
            $param = $this->request()->getRequestParam();
            if (($errMsg = $this->validateDisplay($param)) !== true) {
                throw new Exception($errMsg, 405);
            }
            $model = $this->model::invoke($client);
            $model->modifyData($param['id'], ['display' => $param['display']]);
            return true;
        });
        $this->afterMofidyDisplay($this->request()->getRequestParam());
        Tools::json($this->response(), 200, [], '数据修改完成');
    }

    /**
     * @description: 验证显示参数
     * @param array $param 前端参数
     * @return {*}
     */
    protected function validateDisplay(array $param)
    {
        return true;
    }

    /**
     * @description: 修改显示属性后
     * @param array $param 前端参数
     * @return {*}
     */
    protected function afterMofidyDisplay(array $param)
    {
        return true;
    }
}