<?php

use EasySwoole\DDL\Blueprint\Create\Table as CreateTable;
use EasySwoole\DDL\Blueprint\Alter\Table as AlterTable;
use EasySwoole\DDL\Blueprint\Drop\Table as DropTable;
use EasySwoole\DDL\DDLBuilder;
use EasySwoole\DDL\Enum\Character;
use EasySwoole\DDL\Enum\Engine;

/**
 * migrate alter
 * Class ArticleContent
 */
class ArticleContent
{
    /**
     * migrate run
     * @return string
     */
    public function up()
    {
        return DDLBuilder::alter('article',function (AlterTable $table){
            $table->addColumn()->tinyint('content_type', 1)->setDefaultValue(0)->setColumnComment('文本类型 0、富文本 1、markdown');
        });
    }

    /**
     * migrate rollback
     * @return string
     */
    public function down()
    {
        return DDLBuilder::alter('article',function (AlterTable $table){
            $table->dropColumn('content_type');
        });
    }
}
